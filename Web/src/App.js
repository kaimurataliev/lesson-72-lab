import React, { Component, Fragment } from 'react';
import { Route, NavLink, Switch } from 'react-router-dom';

import Layout from './components/UI/Layout/Layout';
import Dishes from './containers/Dishes/Dishes';
import AddNewDish from './containers/AddNewDish/AddNewDish/AddNewDish';
import './App.css';
import Edit from "./containers/Edit/Edit";
import Orders from "./containers/Orders/Orders";

class App extends Component {
  render() {
    return (
        <Layout>
          <Switch>
            <Route path="/" exact component={Dishes}/>
            <Route path="/addNewDish" component={AddNewDish}/>
            <Route path="/orders" component={Orders}/>
            <Route path="/edit/:id" component={Edit}/>
          </Switch>
        </Layout>
    );
  }
}

export default App;
