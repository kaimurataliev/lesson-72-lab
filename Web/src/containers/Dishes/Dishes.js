import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {deleteDishFromBase, fetchDishes, fetchOrders} from "../../store/actions";
import { NavLink } from 'react-router-dom';

import Dish from '../../components/Dish/Dish';
import './Dishes.css';

class Dishes extends Component {

    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchOrders();
    }

    deleteDishHandler = (event, dish) => {
        event.preventDefault();
        this.props.deleteDishFromBase(dish);
        this.props.fetchDishes();
    };


    render() {
        return (
            <div className="container">
                <h2>Меню</h2>
                <NavLink className='addNewDish' to="/addNewDish">Добавить блюдо</NavLink>
                <Dish
                    dish={this.props.dishes}
                    deleteDishHandler={this.deleteDishHandler}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dishes
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDishes: () => dispatch(fetchDishes()),
        deleteDishFromBase: (dish) => dispatch(deleteDishFromBase(dish)),
        fetchOrders: () => dispatch(fetchOrders())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);