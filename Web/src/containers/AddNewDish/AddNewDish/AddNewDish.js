import React, { Component, Fragment } from 'react';
import './AddNewDish.css';
import { connect } from 'react-redux';
import {addDishToBase} from "../../../store/actions";

class AddNewDish extends Component {

    state = {
        name: '',
        price: '',
        image: ''
    };

    getName = (event) => {
        this.setState({name: event.target.value})
    };

    getPrice = (event) => {
        this.setState({price: event.target.value});
    };

    getImageUrl = (event) => {
        this.setState({image: event.target.value})
    };

    sendDishHandler = event => {
        event.preventDefault();
        this.props.addDishToBase(this.state);
        this.setState({name: '', price: '', image: ''});
    };

    render() {
        return (
            <Fragment>
                <form className="form">
                    <h1>Добавьте новое блюдо</h1>
                    <input
                        value={this.state.name}
                        onChange={this.getName}
                        type="text"
                        placeholder="Введите название"
                    />

                    <input
                        value={this.state.price}
                        onChange={this.getPrice}
                        type="text"
                        placeholder="Введите цену"
                    />

                    <input
                        value={this.state.image}
                        onChange={this.getImageUrl}
                        type="text"
                        placeholder="Введите ссылку на картинку"
                    />

                    <button onClick={this.sendDishHandler}>Добавить</button>

                 </form>
            </Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addDishToBase: (data) => dispatch(addDishToBase(data))
    }
};

export default connect(null, mapDispatchToProps)(AddNewDish);