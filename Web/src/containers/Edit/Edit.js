import React, { Component } from 'react';
import './Edit.css';
import { connect } from 'react-redux';
import {editDish, getInfo} from "../../store/actions";

class Edit extends Component {

    state = {
        name: '',
        price: '',
        image: ''
    };

    componentDidMount() {
        this.props.getInfo(this.props.match.params.id);
    }

    componentWillReceiveProps(props) {
        if(props.dish.name === this.state.name && props.dish.price === this.state.price && props.dish.image === this.state.image) {
            return
        }
        this.setState({
            name: props.dish.name,
            price: props.dish.price,
            image: props.dish.image
        })
    };

    getName = (event) => {
        event.preventDefault();
        this.setState({name: event.target.value});
    };

    getPrice = (event) => {
        event.preventDefault();
        this.setState({price: event.target.value});
    };

    getImage = (event) => {
        event.preventDefault();
        this.setState({image: event.target.value});
    };

    editHandler = (event, id, data) => {
        event.preventDefault();
        this.props.editDish(id, data);
        this.setState({
            name: '',
            price: '',
            image: ''
        })
    };

    render() {

        const id = this.props.match.params.id;
        return (
            <form className="edit">
                <h1>Редактирование блюда</h1>
                <input type="text"
                       value={this.state.name}
                       onChange={this.getName}
                />
                <input type="text"
                       value={this.state.price}
                       onChange={this.getPrice}
                />
                <input type="text"
                       value={this.state.image}
                       onChange={this.getImage}
                />
                <button onClick={(event) => this.editHandler(event, id, this.state)}>Редактировать</button>
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        dish: state.dish
    }
};

const mapDispatchToProps = dispatch => {
    return {
        editDish: (id, data) => dispatch(editDish(id, data)),
        getInfo: (id) => dispatch(getInfo(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);