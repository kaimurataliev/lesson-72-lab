import React, { Component, Fragment } from 'react';
import './Orders.css';
import { connect } from 'react-redux';
import {fetchOrders, fetchDishes} from "../../store/actions";

class Orders extends Component {

    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchOrders();
    }

    render() {

        const menu = this.props.menu;
        const orders = this.props.orders;
        console.log(menu, "Menu");
        console.log(orders, "Orders");
        return (
            <div className="orders">
                {/*{Object.keys(menu).length && Object.keys(orders).length ? Object.keys(orders).map((dishKey, index) => {*/}
                    {/*return (*/}
                        {/*<div className="order" key={index}>*/}
                            {/*<h3>Название блюда: {menu[dishKey].name}</h3>*/}
                            {/*<p>Цена: {menu[dishKey].price}</p>*/}
                            {/*<span>Количество: {orders[dishKey]}</span>*/}
                            {/*<i>Доставка: 150 сом</i>*/}
                            {/*<h4>Общая цена: </h4>*/}
                        {/*</div>*/}
                    {/*)*/}
                {/*}) : null}*/}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        orders: state.orders,
        menu: state.dishes
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchOrders: () => dispatch(fetchOrders()),
        fetchDishes: () => dispatch(fetchDishes())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);