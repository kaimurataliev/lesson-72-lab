import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.css';

const Header = (props) => {
    return (
        <header className="Toolbar">
            <nav>
                <NavLink className="Link" to="/">Меню</NavLink>
                <NavLink className="Link" to="/orders">Заказы</NavLink>
            </nav>
        </header>
    )
};

export default Header;