import React from 'react';
import './Dish.css';
import { NavLink } from 'react-router-dom';

const Dish = props => {

    const dishes = props.dish? Object.keys(props.dish): null;
    return dishes?
        dishes.map((dish, index) => {
        const link = `/edit/${dish}`;
            return (
                <div className="dish" key={index}>
                    <img src={props.dish[dish].image} alt="img"/>
                    <h4>Название: {props.dish[dish].name}</h4>
                    <p>Цена: {props.dish[dish].price} сом</p>
                    <NavLink className='edit-btn' to={link}>Редактировать</NavLink>
                    <button onClick={(event) => props.deleteDishHandler(event, dish)}>Удалить</button>
                </div>
            )
        }): <h3>Нет блюд в меню</h3>;
};

export default Dish;