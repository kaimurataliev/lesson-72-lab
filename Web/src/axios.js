import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://homework-65-f8250.firebaseio.com/'
});


export default instance;