import * as actions from './actions';

const initialState = {
    dish: {},
    dishes: {},
    orders: {},
    isLoading: false,
    error: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.FETCH_REQUEST:
            return {...state, isLoading: true};

        case actions.FETCH_ERROR:
            return {...state, error: true};

        case actions.FETCH_MENU_SUCCESS:
            return {...state, dishes: action.data};

        case actions.FETCH_DISH:
            return {...state, dish: action.data};

        case actions.FETCH_ORDERS:
            return {...state, orders: action.data};

        default:
            return state;
    }
};

export default reducer;
