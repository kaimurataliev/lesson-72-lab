import axios from '../axios';

export const FETCH_MENU_SUCCESS = "FETCH_MENU_SUCCESS";
export const FETCH_REQUEST = "FETCH_REQUEST";
export const FETCH_ERROR = "FETCH_ERROR";
export const FETCH_DISH = "FETCH_DISH";
export const FETCH_ORDERS = "FETCH_ORDERS";


export const fetchRequest = () => {
    return {type: FETCH_REQUEST}
};

export const fetchError = () => {
    return {type: FETCH_ERROR}
};

export const fetchDishSuccess = (data) => {
    return {type: FETCH_DISH, data};
};

export const fetchMenuSuccess = (data) => {
    return {type: FETCH_MENU_SUCCESS, data};
};

export const fetchOrdersSuccess = data => {
    return {type: FETCH_ORDERS, data}
};

export const fetchOrders = () => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.get('./orders.json')
            .then(response => {
                dispatch(fetchOrdersSuccess(response.data));
            })
    }
};

export const addDishToBase = (data) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.post('./dishes.json', data)
    }
};

export const fetchDishes = () => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.get('./dishes.json')
            .then(response => {
                dispatch(fetchMenuSuccess(response.data));
            }, error => {
                dispatch(fetchError(error))
            })
    }
};

export const deleteDishFromBase = (dish) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.delete(`./dishes/${dish}.json`)
            .then(dispatch(fetchDishes()))
    }
};

export const editDish = (id, data) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.put(`./dishes/${id}.json`, data);
    }
};

export const getInfo = (id) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.get(`./dishes/${id}.json`)
            .then(response => {
                dispatch(fetchDishSuccess(response.data));
            })
    }
};